package com.lessons;

import java.util.Locale;

public class Main {

    public static void main(String[] args) {
        int a = 3;
        int b = 15;
        int c = 8;
        double x1;
        double x2;

        // ax^2 + bx + c = 0.
        double D = Math.pow(b,2) - 4 * a * c;
        x1 = ((-b) - Math.sqrt(D)) / (2 * a);
        x2 = ((-b) + Math.sqrt(D)) / (2 * a);

        System.out.printf(Locale.getDefault(), "%dx^2 + %dx + %d = 0;\n", a, b, c);
        System.out.printf(Locale.getDefault(), "x1 = %.3f;\n", x1);
        System.out.printf(Locale.getDefault(), "x2 = %.3f;\n", x2);
    }
}
